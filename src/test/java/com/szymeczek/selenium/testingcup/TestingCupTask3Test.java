package com.szymeczek.selenium.testingcup;

import com.szymeczek.selenium.testingcup.page.Task3Page;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public class TestingCupTask3Test {
    WebDriver webDriver;

    @BeforeEach
    void setUp() {
        WebDriverManager.chromedriver().setup();
        webDriver = new ChromeDriver();

        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    void after_edit_personal_data_should_visible_message() {
        Task3Page page = new Task3Page(webDriver).get();
        page
                .openEditMode()
                .cleanAndSetFistName("Krzysztof")
                .cleanAndSetNotes("Notatka")
                .cleanAndSetPhone("555555555")
                .cleanAndSetLastName("Szymeczek")
//                .fileSelect("src/test/resources/upload.png")
                .save();
        String notificationMessage = page.getNotification();
        Assertions.assertEquals("Twoje dane zostały poprawnie zapisane",
                notificationMessage);
    }
}
