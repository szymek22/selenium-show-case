package com.szymeczek.selenium.testingcup.page;

import com.szymeczek.selenium.testingcup.page.components.ProductInfoComponent;
import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

@Getter
public class ShopProductsPageList extends LoadableComponent<ShopProductsPageList> {
    private final WebDriver webDriver;
    @FindBy(className = "img-circle")
    private List<WebElement> draggableIcons;

    @FindBy(css = ".thumbnail input[type='number']")
    private List<WebElement> unitValues;
    @FindBy(className = "summary-quantity")
    private WebElement summaryQuantity;

    @FindBy(css = "[data-add-to-basket]")
    private List<WebElement> buttonsAddToBasket;


    @FindBy(className = "thumbnail")
    private List<WebElement> productsInfoComponents;


    public ShopProductsPageList(WebDriver webDriver) {
        this.webDriver = webDriver;
        load();
//                PageFactory.initElements(new AjaxElementLocatorFactory(webDriver,
//                5), this);
        PageFactory.initElements(webDriver, ProductInfoComponent.class);
    }

    @Override
    protected void load() {
        this.webDriver.get("https://testingcup.pgs-soft.com/task_7");
    }

    @Override
    protected void isLoaded() throws Error {

    }

    public void addProductDragAndDrop(int id, int expectedBasketValue) {
        WebElement productIcon = loadThumbProductComponent(id)
                .getCircleElement();
        new Actions(webDriver).clickAndHold(productIcon)
                .moveByOffset(20, 20)
                .moveToElement(webDriver.findElement(
                        By.className("panel-info")))
                .moveToElement(webDriver.findElement(
                        By.cssSelector("div[data-basket-body] .place-to-drop")))
                .release().build().perform();

        new WebDriverWait(webDriver, 2)
                .until(ExpectedConditions
                        .textToBePresentInElement(summaryQuantity, String.valueOf(expectedBasketValue)));
    }

    public ProductInfoComponent loadThumbProductComponent(int id) {
        productsInfoComponents.get(id);
        return new ProductInfoComponent(productsInfoComponents.get(id));
    }

    public void setUnitNumber(int i, int value) {
        unitValues.get(i).clear();
        unitValues.get(i).sendKeys(String.valueOf(value));
    }
}
