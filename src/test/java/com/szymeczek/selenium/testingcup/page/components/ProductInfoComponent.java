package com.szymeczek.selenium.testingcup.page.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ProductInfoComponent {
    private WebElement webElement;
    private By imgCircle = By.className("img-circle");

    public ProductInfoComponent(WebElement webElement) {
        this.webElement = webElement;
    }

    public WebElement getCircleElement() {
        return webElement.findElement(imgCircle);
    }

}
