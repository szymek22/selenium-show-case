package com.szymeczek.selenium.testingcup.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Task3Page extends LoadableComponent<Task3Page> {
    @FindBy(id = "in-name")
    private WebElement firstName;
    @FindBy(id = "in-surname")
    private WebElement surname;
    @FindBy(id = "in-notes")
    private WebElement notes;
    @FindBy(id = "in-phone")
    private WebElement inPhone;
    @FindBy(id = "in-file")
    private WebElement inFile;
    @FindBy(id = "save-btn")
    private WebElement save;

    @FindBy(css = "[data-notify-text]")
    private WebElement notificationMessage;

    private WebDriver webDriver;

    public Task3Page(WebDriver webDriver) {

        this.webDriver = webDriver;
        load();
        PageFactory.initElements(webDriver, this);
    }

    @Override
    protected void load() {
        webDriver.get("https://buggy-testingcup.pgs-soft.com/task_3");
    }

    public Task3Page openEditMode() {
        webDriver.findElement(By.cssSelector("[data-toggle='dropdown']")).click();

        webDriver.findElement(By.id("menu1"))
                .findElement(By.partialLinkText("Formularz")).click();

        webDriver.findElement(By.id("start-edit")).click();
        return this;
    }

    public Task3Page cleanAndSetFistName(String value) {
        firstName.clear();
        firstName.sendKeys(value);
        return this;
    }

    public Task3Page cleanAndSetLastName(String value) {
        surname.clear();
        surname.sendKeys(value);
        return this;
    }

    public Task3Page cleanAndSetNotes(String value) {
        notes.clear();
        notes.sendKeys(value);
        return this;
    }

    public Task3Page cleanAndSetPhone(String value) {
        inPhone.clear();
        inPhone.sendKeys(value);
        return this;
    }

    public Task3Page save() {
        save.click();
        return this;
    }

    public Task3Page fileSelect(String value) {
        inFile.sendKeys(value);
        return this;
    }


    @Override
    protected void isLoaded() throws Error {

    }

    public WebElement getFirstName() {
        return firstName;
    }

    public WebElement getNotificationMessage() {
        new WebDriverWait(webDriver, 3)
                .until(ExpectedConditions
                        .visibilityOf(notificationMessage));
        return notificationMessage;
    }

    public String getNotification() {

        try {
            return getNotificationMessage().getText();
        } catch (Exception e) {
            return "";
        }

    }
}
