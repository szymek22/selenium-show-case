package com.szymeczek.selenium.testingcup;

import com.szymeczek.selenium.testingcup.page.ShopProductsPageList;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class TestingCubTask7Test {
    WebDriver webDriver;

    @BeforeEach
    void setUp() {
        WebDriverManager.chromedriver().setup();
        webDriver = new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    void drag_and_drop_first_position_product() {
        ShopProductsPageList pageProducts = new ShopProductsPageList(webDriver).get();
        pageProducts.setUnitNumber(1, 3);
        pageProducts.addProductDragAndDrop(1, 3);
        Assertions.assertEquals("3", pageProducts.getSummaryQuantity().getText());
    }

    @Test
    void drag_and_drop_many_positions_product() throws InterruptedException {
        ShopProductsPageList pageProducts = new ShopProductsPageList(webDriver).get();
        Assertions.assertTrue(pageProducts.getDraggableIcons().size() > 2, "Wrong size init data products");
        pageProducts.setUnitNumber(1, 3);
        pageProducts.addProductDragAndDrop(1, 3);

        pageProducts.setUnitNumber(2, 2);
        pageProducts.addProductDragAndDrop(2, 5);
        Assertions.assertEquals("5", pageProducts.getSummaryQuantity().getText());
    }

    @Test
    void mixed_mode_move_to_basket_product() throws InterruptedException {
        ShopProductsPageList pageProducts = new ShopProductsPageList(webDriver).get();
        Assertions.assertTrue(pageProducts.getDraggableIcons().size() > 2, "Wrong size init data products");
        pageProducts.setUnitNumber(1, 3);
        pageProducts.addProductDragAndDrop(1, 3);
        pageProducts.setUnitNumber(2, 3);
        pageProducts.getButtonsAddToBasket().get(2).click();
        Assertions.assertEquals("6", pageProducts.getSummaryQuantity().getText());
    }

    @AfterEach
    void tearDown() {
        webDriver.close();
    }
}
