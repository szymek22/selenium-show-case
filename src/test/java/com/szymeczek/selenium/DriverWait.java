package com.szymeczek.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class DriverWait {
    static WebDriverWait crateDriverWait(WebDriver driver) {
        return new WebDriverWait(driver, 20);
    }
}
