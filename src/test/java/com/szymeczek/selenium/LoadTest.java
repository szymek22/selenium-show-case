package com.szymeczek.selenium;

import com.github.javafaker.Faker;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoadTest {

    public static final String BASE_HOST = "http://automationpractice.com";
    public static final String T_SHIRTS_PAGE_TITLE = "T-shirts - My Store";
    public static final String T_SHIRTS_MENU_OPTION_TEXT = "T-shirts";
    private final Creator creator = new Creator();
    private WebDriver driver;

    @BeforeEach
    void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = creator.createDriver();
    }



    @Test
    void load_and_check_title() {
        driver.get(BASE_HOST + "/");
        Assertions.assertEquals("My Store", driver.getTitle());
    }

    @Test
    void register_user() {
        driver.get(BASE_HOST + "/index.php?controller=authentication&back=my-account");

        String registerEmail = "ziutek.ziutkowski" +
                new Faker().animal().name().replace(" ", "") +
                "@selenium.pl";

        driver.findElement(By.id("email_create"))
                .sendKeys(registerEmail);
        driver.findElement(By.id("SubmitCreate")).sendKeys(Keys.ENTER);
        DriverWait.crateDriverWait(driver)
                .until(
                        ExpectedConditions.textToBe(
                                By.className("page-subheading"),
                                "YOUR PERSONAL INFORMATION"
                        )
                );
        WebElement id_gender1 = driver.findElement(By.id("id_gender1"));
        id_gender1.click();
        driver.findElement(By.id("customer_firstname")).sendKeys("Ziutek");
        driver.findElement(By.id("customer_lastname")).sendKeys("Ziutkowski");
        driver.findElement(By.id("passwd")).sendKeys("qwert1234");
        new Select(driver.findElement(By.id("days"))).selectByIndex(10);
        new Select(driver.findElement(By.id("months"))).selectByIndex(2);
        new Select(driver.findElement(By.id("years"))).selectByIndex(3);
        driver.findElement(By.id("uniform-newsletter")).click();
        driver.findElement(By.id("uniform-optin")).click();
        driver.findElement(By.id("address1")).sendKeys("Aleja Juliusza Słowackiego 14");
        driver.findElement(By.id("city")).sendKeys("Krakow");
        new Select(driver.findElement(By.id("id_state"))).selectByIndex(2);
        driver.findElement(By.id("postcode")).sendKeys("00000");
        new Select(driver.findElement(By.id("id_country"))).selectByIndex(1);
        driver.findElement(By.id("phone_mobile")).sendKeys("555555555");


        String email = driver.findElement(By.id("email")).getAttribute("value");
        driver.findElement(By.id("submitAccount")).click();
//        driver.findElement(By.cssSelector("#submitAccount input")).click();
//        driver.findElement(By.cssSelector("#create-account_form  .page-subheading")).click();
//        driver.findElement(By.cssSelector("#create-account_form .page-subheading")).click();
//        driver.findElement(By.cssSelector("H3.page-subheading")).click();
//        driver.findElement(By.cssSelector("H3[value='email']  .page-subheading")).click();

        DriverWait.crateDriverWait(driver)
                .until(ExpectedConditions
                        .visibilityOf(driver.findElement(By.cssSelector("#my-account .page-heading")))
                );

        Assertions.assertEquals(BASE_HOST + "/index.php?controller=my-account", driver.getCurrentUrl());
        Assertions.assertEquals(registerEmail, email, "Email should be equals with email from first step");

    }

    @Test
    void should_open_submenu() {
        WebDriverWait webDriverWait = DriverWait.crateDriverWait(driver);
        driver.get(BASE_HOST);
        new Actions(driver)
                .moveToElement(driver.findElement(By.partialLinkText("WOMEN")))
                .build().perform();
        webDriverWait.until(
                ExpectedConditions.visibilityOfElementLocated(By.linkText(T_SHIRTS_MENU_OPTION_TEXT))
        );
        Action moveCursorAction = new Actions(driver)
                .moveToElement(
                        driver.findElement(By.linkText(T_SHIRTS_MENU_OPTION_TEXT))
                )
                .click().build();
        moveCursorAction.perform();

        webDriverWait.until(d -> T_SHIRTS_PAGE_TITLE.equals(d.getTitle()));
        Assertions.assertEquals( T_SHIRTS_PAGE_TITLE, driver.getTitle());
    }

    @AfterEach
    void tearDown() {
//        Thread.sleep(20000);
        driver.close();
    }
}
